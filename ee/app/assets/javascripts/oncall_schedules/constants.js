export const LENGTH_ENUM = {
  hours: 'hours',
  days: 'days',
  weeks: 'weeks',
};

export const CHEVRON_SKIPPING_SHADE_ENUM = ['500', '600', '700', '800', '900', '950'];

export const CHEVRON_SKIPPING_PALETTE_ENUM = ['blue', 'orange', 'aqua', 'green', 'magenta'];
